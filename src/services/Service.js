import axios from 'axios';

class Service {

    constructor() {
        this.axios = axios.create({
          baseURL: 'https://gateway.marvel.com:443/v1/public',
          params: {
            apikey: '42b350a2800ca27bf6e6ade42e356ec5',
            hash: '527203cd9ec9456a3040394acdc14c2d',
            ts: 1
          },
          headers: {
            'Accept': 'application/json'
          }
        });
    }

    // AJAX
    getApiMarvelComics() {
        return this.axios.get('/comics?orderBy=title');
    }

    getSingleComic(comicID) {
      return this.axios.get(`/comics/${comicID}`);
    }

}

export default {
  service: new Service()
};
