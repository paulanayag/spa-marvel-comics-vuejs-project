import Vue from 'vue'
import Router from 'vue-router'
import NotFound from '../views/NotFound';
import store from '../store/index';

// Components
import Home from '@/components/Home'
import ComicDetail from '@/components/ComicDetail'

Vue.use(Router)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/detail/:comicID',
    name: 'ComicDetail',
    component: ComicDetail,
    beforeEnter(to, from, next) {
      if (store.state.currentComic === null || !store.state.currentComic.hasOwnProperty('id')) {
        console.log('No comic in state');
        store.state.services.service.getSingleComic(to.params.comicID)
          .then(comic => {
          store.dispatch('setCurrentComic', comic.data.data.results[0]);
          next();
          })
          .catch(err => {
          console.error(err);
          next('/404');
          });
      } else {
        next();
      }
    }
  },
  {
    path: '/404',
    name: '404',
    component: NotFound
  },
  {
    path: '*',
    redirect: '/404'
  }
]

export default new Router({
  routes
})
