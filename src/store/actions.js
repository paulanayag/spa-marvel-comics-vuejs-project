export default {
  setCurrentComic(context, comic) {
    context.commit('SET_CURRENT_COMIC', comic);
  },
  setFavComic(context, favComic) {
    context.commit('SET_FAV_COMIC', favComic);
  }
}
