import Axios from 'axios'
import service from '../services/Service'

// Axios Configuration
Axios.defaults.headers.common.Accept = 'application/json'

export default {
    service: new service(Axios)
}