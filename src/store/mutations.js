export default {
  SET_CURRENT_COMIC(state, comic) {
    state.currentComic = comic;
  },
  SET_FAV_COMIC(state, favComic) {
    state.favComics.push(favComic);
    window.localStorage.setItem('arrayFavComics', JSON.stringify(state.favComics));
  }
}
