// Configuración de Vuex para guardar todo aquello que va a ser compartido
import Vue from 'vue'
import Vuex from 'vuex'

import services from '../services/Service';
import mutations from './mutations';
import actions from './actions';

Vue.use(Vuex)

const state = {
    services,
    currentComic: {},
    favComics: []
}

export default new Vuex.Store({
  state,
  mutations,
  actions
})
