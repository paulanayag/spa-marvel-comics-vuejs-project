It is a SPA using VueJS which consumes the official Marvel's API.\

To build this SPA with VueJS, I have used the official router which is Vue-Router.\
Vue-Router allows to navigate through URL and renders components in the <router-view> depends on the route\

To navigate between components, I have used <router-link>\
    Navigation VueJS here: https://router.vuejs.org/guide/#html\
    
To store commun components, I have created a store folder that uses Vuex (a state management pattern).\
Vuex allows to share sources during the lifecycle of the application.\
    Vuex documentation here: https://vuex.vuejs.org/\

To make AJAX requests, I have used the Axios library.\

The interface user kit used in this project is ElementUI\
    ElementUI documentation here: https://element.eleme.io/#/es\


To run this project you have to:\
    1.- Install all the dependencies with the command --> npm install\
    2.- Run the SPA in a local server with the command --> npm run dev\

To push the project to a server you have to:\
    1.- Write on the terminal --> npm run build\
    2.- It generates automatically the bundle folder\
    3.- Push the folder bundle to remote repository\


--> API documentation\
    1.- You have to create an account on the website: developer.marvel.com\
    2.- You have to obtain an API key\
    3.- Now you are allowed to consume the official Marvel's API\

All the documentation is on the official website.\
In this application, I have used the GET called with the following endpoint: /v1/public/comics\
This url returns the list of comics.\
The url used in this project includes the method orderBy title\


There is an base SPA project in the following website: https://anexsoft.com/proyecto-base-spa-con-vue-vuex-vuerouter-axios-y-element-ui\
